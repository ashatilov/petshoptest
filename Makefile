up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build rating-composer
docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

rating-composer:
	docker-compose run --rm rating-php-cli composer install
run:
	docker-compose run --rm rating-php-cli php bin/console app:rating