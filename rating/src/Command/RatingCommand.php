<?php
namespace App\Command;

use App\Model\Team;
use App\Model\TeamList;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class RatingCommand extends Command
{
    protected static $defaultName = 'app:rating';

    protected function configure()
    {
        $this->setDescription('Тестовое задание php');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $defaultJson = '[  { "team": "Axiom", "scores": 88 },   { "team": "BnL", "scores": 65 },   { "team": "Eva", "scores": 99 },   { "team": "WALL-E", "scores": 99 } ]';

        $output->writeln('Default json from endpoint: '.$defaultJson);
        $helper = $this->getHelper('question');
        $question = new Question('Please enter real endpoint: ', $defaultJson);
        $endpointJson = $defaultJson;
        $endpoint = $helper->ask($input, $output, $question);

        if ($endpoint!=$defaultJson) {
            $client = new Client();
            $response = $client->request('GET', $endpoint);
            $endpointJson = (string) $response->getBody();
        }

        $teamList = new TeamList();
        $teamList->setTeamsAsJson($endpointJson);

        $output->writeln($teamList->getTeamsAsJson());
        return 0;
    }
}