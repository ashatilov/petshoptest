<?php


namespace App\Model;


class Team
{
    public $rank;
    public $team;
    public $scores;
    public function __construct(string $team, int $scores)
    {
        $this->team = $team;
        $this->scores = $scores;
    }

    public function setRank(int $rank)
    {
        $this->rank = $rank;
    }
}