<?php


namespace App\Model;


class RankHelper
{
    /**
     * @var array
     */
    private $scoresArr = [];
    private $rank = 0;

    public function getPosition(int $scores)
    {
        $this->rank++;
        if(key_exists($scores, $this->scoresArr))
        {
            return $this->scoresArr[$scores];
        }
        $this->scoresArr[$scores] = $this->rank;
        return $this->rank;
    }
}