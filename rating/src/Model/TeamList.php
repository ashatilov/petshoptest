<?php


namespace App\Model;



class TeamList
{
    private $teams = [];

    public function addTeam(Team $team)
    {
        if(in_array($team->team, $this->teams))
        {
            throw new \DomainException("Error: team already exists");
        }
        $this->teams[] = $team;
    }

    public function getTeams(): array
    {
        return $this->teams;
    }

    public function setTeamsAsJson(string $json): void
    {
        $endpoints = json_decode($json);
        foreach ($endpoints as $item)
        {
            $this->addTeam(new Team($item->team, $item->scores));
        }
    }

    public function getTeamsAsJson()
    {
        usort($this->teams, function ($a, $b){
            if ($a->scores == $b->scores) {
                return 0;
            }
            return ($a->scores < $b->scores) ? 1 : -1;
        });
        /**
         * @var Team $team
         */
        $rankHelper = new RankHelper();
        foreach ($this->teams as $team)
        {
            $team->setRank($rankHelper->getPosition($team->scores));
        }
        return json_encode($this->teams);
    }

}