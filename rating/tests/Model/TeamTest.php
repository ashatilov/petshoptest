<?php

namespace Model;

use App\Model\Team;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{

    public function testTeam()
    {
        $team = new Team('name', 12);
        $team->setRank(1);
        $this->assertEquals($team->team,'name');
        $this->assertEquals($team->scores, 12);
        $this->assertEquals($team->rank, 1);
    }
}
