<?php

namespace Model;

use App\Model\RankHelper;
use PHPUnit\Framework\TestCase;

class RankHelperTest extends TestCase
{

    public function testGetPosition()
    {
        $rankHelper = new RankHelper();
        $scores = [9, 8, 8, 7];
        $equalsRanks = [1, 2, 2, 4];
        foreach ($scores as $key=>$score)
        {
            $rank = $rankHelper->getPosition($score);
            $this->assertEquals($rank, $equalsRanks[$key]);
        }
    }
}
