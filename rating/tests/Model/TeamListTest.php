<?php

namespace Model;

use App\Model\Team;
use App\Model\TeamList;
use PHPUnit\Framework\TestCase;

class TeamListTest extends TestCase
{

    public function testSetTeamsAsJson()
    {
        $json = '[{"team":"Eva","scores":99},{"team":"WALL-E","scores":99},{"team":"Axiom","scores":88},{"team":"BnL","scores":65}]';
$actual = [
    0 => new Team('Axiom',88),
    1 => new Team('BnL',65),
    2 => new Team('Eva',99),
    3 => new Team('WALL-E',99),
];
        $teamList = new TeamList();
        $teamList->setTeamsAsJson($json);
        $this->assertEqualsCanonicalizing($teamList->getTeams(), $actual);
    }

    public function testGetTeamsAsJson()
    {
        $json = '[{"team":"Eva","scores":99},{"team":"WALL-E","scores":99},{"team":"Axiom","scores":88},{"team":"BnL","scores":65}]';
        $actual = '[{"rank":1,"team":"Eva","scores":99},{"rank":1,"team":"WALL-E","scores":99},{"rank":3,"team":"Axiom","scores":88},{"rank":4,"team":"BnL","scores":65}]';
        $teamList = new TeamList();
        $teamList->setTeamsAsJson($json);
        $this->assertEquals($teamList->getTeamsAsJson(), $actual);
    }
}
